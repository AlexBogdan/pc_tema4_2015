#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void get_cmd (char (*parametrii)[128] , char *cmd);
void drename (char *path , char *newname);
void  ddelete (char *path);
void dcreate (char *path);
void dprint  (char *path);
void frename (char *path , char *newname);
void fdelete (char *path);
void fcreate (char *path);
void pformat (char *path);
void pdelete (char *path);
void pcreate (char *path , int d , int f);

typedef struct task1
{
	char nume[9];
	int total;
	int folosit;
}task1;

typedef struct cap
{
	int size;
	char nume[8];
}header;

typedef struct fisier
{
	header head;
}file;

typedef struct director
{
	header head;
}dir;

typedef struct partitie
{
	header head;
	int maxd;
	int maxf;
}part;

void get_cmd (char (*parametrii)[128] , char *cmd)
{
	char  *token , delimitator[] = " \n";
	int i = 0;
	token = strtok(cmd, delimitator);

  	while( token != NULL )
 	{
  		strcpy (parametrii[i] , token);
  		i++;

  		token = strtok(NULL, delimitator);
   	}
}

void drename (char *path , char *newname)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	dir  temp_dir;
	int nr_parts , ji , i , point_part;
	char path1[16] , path2[1024] , cmp[1024];
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);

	if (strlen(newname) > 8 || strlen(path2) > 8)
	{
		printf("INVALID\n");
		fclose(mbr);
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if(point_part == 0) continue;
		
		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);
				temp_dir.head.nume[temp_dir.head.size] = '\0';
				if (temp_dir.head.size == 0)
				{
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
					continue;
				}

				strcpy (cmp , temp_dir.head.nume); 
				cmp[temp_dir.head.size] = '\0';
				if (strcmp (cmp , path2) == 0)
				{
					char c[12]; 
					memset (c , 0 , 12);
					fseek(mbr , -12 , SEEK_CUR);
					fwrite(c , sizeof(char) , 12 , mbr);

					int l = strlen (newname);
					fseek(mbr , -12 , SEEK_CUR);
					fwrite(&l , sizeof(int) , 1 , mbr);
					fwrite(newname , strlen (newname) , sizeof(char) , mbr);
					printf("SUCCES\n");
				
					fclose(mbr);
					return ;
				}
					else fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void ddelete (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	dir  temp_dir;
	int nr_parts , ji , i , k , point_part;
	char path1[16] , path2[256] , cmp[256];
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);

	if (mbr == NULL) return;

	if (strlen(path) <= 9 || strlen(path2) > 8)
	{
		printf("INVALID\n");
		fclose (mbr);
		return;
	}
	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);
				if (temp_dir.head.size == 0)
				{
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
					continue;
				}

				strcpy (cmp , temp_dir.head.nume); 
				cmp[temp_dir.head.size] = '\0';
				if (strcmp (cmp , path2) == 0)
				{	
					int l = 12*(temp_part.maxf + 1); 
					char c[l]; 
					memset (c , 0 , l);
					fseek(mbr , -12 , SEEK_CUR);

					// Stergem si fisierele din folder , nu numai folderul
					fwrite(c , sizeof(char) , l , mbr);

					printf("SUCCES\n");
					fclose(mbr);
					return ;
				}
				else	fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void dcreate (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	dir  temp_dir;
	int nr_parts , ji , i , point_part;
	char path1[16] , path2[1024] , cmp[1024];
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);

	if (strlen(path) <= 9 || strlen(path2) > 8)
	{
		printf("INVALID\n");
		fclose (mbr);
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);

				if (temp_dir.head.size == 0)
				{
					fseek (mbr , -12 , SEEK_CUR);
					int l = strlen (path2);
					fwrite(&l , sizeof(int) , 1 , mbr);
					fwrite(path2 , strlen (path2) , sizeof(char) , mbr);
					
					char c[12*temp_part.maxf];
					memset (c , 0 , 12*temp_part.maxf);
					fwrite(c , sizeof(char) , 12*temp_part.maxf , mbr);

					printf("SUCCES\n");
					fclose(mbr);
					return ;
				}
				else
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void dprint (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "r");
	part temp_part;
	dir  temp_dir;
	file temp_file;
	int nr_parts , ji , i , point_part , k;
	char path1[16] , path2[1024] , cmp[1024];
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);

	if (strlen(path) <= 9 || strlen(path2) > 8)
	{
		printf("INVALID\n");
		fclose (mbr);
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);
				strcpy (cmp , temp_dir.head.nume); 
				cmp[temp_dir.head.size] = '\0';
				if (strcmp (cmp , path2) == 0)
				{
					for (k = 1 ; k <= temp_part.maxf ; k++)
					{
						fread(&temp_file , sizeof(file) , 1 , mbr);
						if (temp_file.head.size != 0)
						{
							strcpy (cmp , temp_file.head.nume); 
							cmp[temp_file.head.size] = '\0';
							puts (cmp);
						}
					}
					fclose (mbr);
					return ;
				}
				else
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void fcreate (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	dir  temp_dir;
	file temp_file;

	int nr_parts , ji , i , k , point_part;
	char path1[16] , path2[1024] , cmp[1024] , path3 [1024] , delim[] = "/" , *token;
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);
	if (strchr(path2 , '/') != NULL )
	{
		token = strtok(path2 , delim);
		strcpy (path3 , token + strlen(token) + 1);
	}
	else memset (path3 , 0 , 20);

	if (strlen(path) <= 9 || strlen (path2) > 8 || strlen (path3) > 8)
	{
		printf("INVALID\n");
		fclose (mbr);
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);

				strcpy (cmp , temp_dir.head.nume); 
				cmp[temp_dir.head.size] = '\0';
				if (strcmp(path2 , cmp) == 0)
				{
					for (k = 1 ; k <= temp_part.maxf ; k++)
					{
						fread(&temp_file , sizeof(file) , 1 , mbr);
						if (temp_file.head.size == 0)
						{	
							char c[12]; 
							memset (c , 0 , 12);
							fseek(mbr , -12 , SEEK_CUR);
							fwrite(c , sizeof(char) , 12 , mbr);

							fseek (mbr , -12 , SEEK_CUR);
							int l = strlen (path3);
							fwrite(&l , sizeof(int) , 1 , mbr);
							fwrite(path3 , strlen (path3) , sizeof(char) , mbr);
					
							printf("SUCCES\n");
							fclose(mbr);
							return ;
						}
					}
				}
				else
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void fdelete (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	dir  temp_dir;
	file temp_file;

	int nr_parts , ji , i , k , point_part;
	char path1[16] , path2[1024] , cmp[1024] , path3 [1024], delim[] = "/" , *token;
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);

	if (strchr(path2 , '/') != NULL )
	{
		token = strtok(path2 , delim);
		strcpy (path3 , token + strlen(token) + 1);
	}
	else memset (path3 , 0 , 20);

	if (strlen(path) <= 9 || strlen (path2) > 8 || strlen (path3) > 8)
	{
		printf("INVALID\n");
		fclose (mbr);
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
		{
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);

				strcpy (cmp , temp_dir.head.nume); 
				cmp[temp_dir.head.size] = '\0';
				if (strcmp(path2 , cmp) == 0)
				{
					for (k = 1 ; k <= temp_part.maxf ; k++)
					{
						fread(&temp_file , sizeof(file) , 1 , mbr);
						if (temp_file.head.size == 0) continue;

						strcpy (cmp , temp_file.head.nume); 
						cmp[temp_file.head.size] = '\0';
						if (strcmp (cmp , path3) == 0)
						{	
							char c[12]; 
							memset (c , 0 , 12);
							fseek(mbr , -12 , SEEK_CUR);
							fwrite(c , sizeof(char) , 12 , mbr);
					
							printf("SUCCES\n");
							fclose(mbr);
							return ;
						}
					}
				}
				else
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
				for (k = 1 ; k <= temp_part.maxf ; k++)
				{
					fread(&temp_file , sizeof(file) , 1 , mbr);
					if (temp_file.head.size == 0) continue;

					strcpy (cmp , temp_file.head.nume); 
					cmp[temp_file.head.size] = '\0';
					if (strcmp (cmp , path2) == 0)
					{	
						char c[12]; 
						memset (c , 0 , 12);
						fseek(mbr , -12 , SEEK_CUR);
						fwrite(c , sizeof(char) , 12 , mbr);
					
						printf("SUCCES\n");
						fclose(mbr);
						return ;
					}
				}
		}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void frename (char *path , char *newname)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	dir  temp_dir;
	file temp_file;

	int nr_parts , ji , i , k , point_part;
	char path1[16] , path2[1024] , cmp[1024] , path3 [1024] , delim[] = "/" , *token;
	strncpy (path1 , path , 8); path1[8] = '\0';
	strcpy (path2 , path + 9);

	if (strchr(path2 , '/') != NULL )
	{
		token = strtok(path2 , delim);
		strcpy (path3 , token + strlen(token) + 1);
	}
	else memset (path3 , 0 , 20);

	if (strlen(path) <= 9 || strlen (path2) > 8
		 || strlen (path3) > 8 || strlen (newname) > 8)
	{
		printf("INVALID\n");
		fclose (mbr);
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path1 , cmp) == 0)
		{
			for (i = 1 ; i<= temp_part.maxd ; i++)
			{
				fread(&temp_dir , sizeof(dir) , 1 , mbr);

				strcpy (cmp , temp_dir.head.nume); 
				cmp[temp_dir.head.size] = '\0';
				if (strcmp(path2 , cmp) == 0)
				{
					for (k = 1 ; k <= temp_part.maxf ; k++)
					{
						fread(&temp_file , sizeof(file) , 1 , mbr);
						if (temp_file.head.size == 0) continue;

						strcpy (cmp , temp_file.head.nume); 
						cmp[temp_file.head.size] = '\0';
						if (strcmp (cmp , path3) == 0)
						{	
							char c[12]; 
							memset (c , 0 , 12);
							fseek(mbr , -12 , SEEK_CUR);
							fwrite(c , sizeof(char) , 12 , mbr);

							fseek(mbr , -12 , SEEK_CUR);
							int l = strlen(newname);
							fwrite(&l , sizeof(int) , 1 , mbr);
							fwrite(newname , strlen (newname) , sizeof(char) , mbr);
							printf("SUCCES\n");

							fclose(mbr);
							return;
						}
					}
				}
				else
					fseek (mbr , 12*temp_part.maxf , SEEK_CUR);
			}
			for (k = 1 ; k <= temp_part.maxf ; k++)
			{
				fread(&temp_file , sizeof(file) , 1 , mbr);
				if (temp_file.head.size == 0) continue;

				strcpy (cmp , temp_file.head.nume); 
				cmp[temp_file.head.size] = '\0';
				if (strcmp (cmp , path2) == 0)
				{	
					char c[12]; 
					memset (c , 0 , 12);
					fseek(mbr , -12 , SEEK_CUR);
					fwrite(c , sizeof(char) , 12 , mbr);
					fseek(mbr , -12 , SEEK_CUR);
					int l = strlen(newname);
					fwrite(&l , sizeof(int) , 1 , mbr);
					fwrite(newname , strlen (newname) , sizeof(char) , mbr);
					printf("SUCCES\n");

					fclose(mbr);
					return;
				}
			}
		}
	}
	printf("INVALID\n");
	fclose(mbr);
}

void pformat (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;

	int nr_parts , ji , point_part;
	char cmp[9];

	if (strlen(path) != 8)
	{
		printf("INVALID\n");
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path , cmp) == 0)
		{
			int dim = 12 * (temp_part.maxd + (temp_part.maxd + 1)*temp_part.maxf );
			char c[dim];
			memset (c , 0 , dim);
			fwrite(&c , sizeof(char) , dim , mbr);

			printf("SUCCES\n");
			fclose (mbr);
			return ;
		}
	}
	printf("INVALID\n");
	fclose (mbr);
}

void pdelete (char *path)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;

	int nr_parts , ji , point_part;
	char cmp[9];

	if (strlen(path) != 8)
	{
		printf("INVALID\n");
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);
		
		if (point_part == 0) continue;

		fseek(mbr , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr);

		strcpy (cmp , temp_part.head.nume); 
		cmp[temp_part.head.size] = '\0';
		if (strcmp(path , cmp) == 0)
		{
			char c[4];
			memset (c , 0 , 4);
			fseek (mbr , ji * 4 , SEEK_SET);
			fwrite(&c , sizeof(char) , 4 , mbr);

			printf("SUCCES\n");
			fclose (mbr);
			return ;
		}
	}
	printf("INVALID\n");
	fclose (mbr);
}

void pcreate (char *path , int d , int f)
{
	FILE *mbr = fopen ("mbr.bin" , "rw+");
	part temp_part;
	int nr_parts , ji , k , point_part , oldpoint = 0, oldsize = 0;
	int need = 20 + 12 * (d + (d+1)*f);

	char cmp[9];

	if (strlen(path) != 8)
	{
		printf("INVALID\n");
		return;
	}

	fread(&nr_parts , sizeof(int) , 1 , mbr);
	int space [nr_parts + 2];
	
	typedef struct task3
	{
		int start;
		int size;
	}task3;

	task3 parts[nr_parts + 1];
	parts[0].size = 0; parts[0].start = 0;

	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		fseek(mbr , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr);

		if (point_part == 0)
		{
			parts[ji].start = 0;
			parts[ji].size = 0;
		}	
		else
		{	
			fseek(mbr , point_part , SEEK_SET);
			fread(&temp_part , sizeof(part) , 1 , mbr);
			parts[ji].start = point_part;
			parts[ji].size = 20 + 12 * (temp_part.maxd + 
				(temp_part.maxd + 1) * temp_part.maxf);
		}
	}
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		if(parts[ji].start == 0)
		{
			for (k = ji ; k <= nr_parts && parts[k].start == 0 ; k++);
			if (parts[k].start - parts[ji-1].start - parts[ji-1].size >= need)
			{
				int x = parts[ji-1].start + parts[ji-1].size;
				fseek (mbr , ji * 4 , SEEK_SET);
				fwrite(&x , sizeof(int) , 1 , mbr);
				printf("%d\n", x);

				fseek (mbr , x , SEEK_SET);
				x = 8; 
				strcpy(cmp , path); cmp[8] = '\0';
				fwrite(&x , sizeof(int) , 1 , mbr);
				fwrite(&cmp , sizeof(char) , 8 , mbr);
				fwrite(&d , sizeof(int) , 1 , mbr);
				fwrite(&f , sizeof(int) , 1 , mbr);

				fclose(mbr);
				return;
			}
			else ji = k;
		}
	}

	printf("INVALID\n");
	fclose (mbr);
}

int main (int argc , char *argv[])
{	
	part temp_part;
	dir  temp_dir;
	file temp_file;
	// -=-=-=-=-=-=-=-=- ~~~ Task 1 ~~~ =-=-=-=-=-=-=-=-=-=-
if (atoi(argv[1]) == 1)
{
	FILE *mbr_file = fopen ("mbr.bin" , "rw+");
	int nr_parts , point_part , ji , i , k , ok = 1 , check;
	fread(&nr_parts , sizeof(int) , 1 , mbr_file);

	task1 output[nr_parts + 1];
	for (ji = 1 ; ji <= nr_parts ; ji++)
	{
		memset (&output[ji] , 0 , sizeof(task1));

		fseek(mbr_file , ji * 4 , SEEK_SET);
		fread(&point_part , sizeof(int) , 1 , mbr_file);
		if (ji != 1)
			if (point_part != check)
			{
				ok = 0;
				break;
			}

		fseek(mbr_file , point_part , SEEK_SET);
		fread(&temp_part , sizeof(part) , 1 , mbr_file);

		strcpy (output[ji].nume , temp_part.head.nume);
		output[ji].nume[8] = '\0';
		output[ji].total = 12 * 
			(temp_part.maxd + (temp_part.maxd + 1)*temp_part.maxf );

		for (i = 1 ; i<= temp_part.maxd ; i++)
		{
			fread(&temp_dir , sizeof(dir) , 1 , mbr_file);
			if (temp_dir.head.size)
				output[ji].folosit += 12;

			for (k = 1 ; k <= temp_part.maxf ; k++)
			{
				fread(&temp_file , sizeof(file) , 1 , mbr_file);
				if (temp_file.head.size)
					output[ji].folosit += 12;
			}
		}
		
		for (k = 1 ; k <= temp_part.maxf ; k++)
			{
				fread(&temp_file , sizeof(file) , 1 , mbr_file);
				if (temp_file.head.size)
					output[ji].folosit += 12;
			}

		check = point_part + 20 + output[ji].total;
	}

	if (ok)
	{
		printf("SUCCES\n");
		for (ji = 1 ; ji <= nr_parts ; ji++)
		{
			fputs(output[ji].nume, stdout);
			printf(" %d %d\n" , output[ji].total , output[ji].folosit);
		}
	}
	else printf("INVALID\n");

	fclose (mbr_file);
}
	// -=-=-=-=-=-=-=-=- ~~~ Task 2 ~~~ =-=-=-=-=-=-=-=-=-=-
if (atoi(argv[1]) == 2)
{	
	FILE *mbr_ops = fopen ("mbr.op" , "r");
	char  params [6][128] , cmd[128];
	
	while (1)
	{	
		memset (params , 0 , 6*128);
		memset (cmd , 0 , 128);
		fgets (cmd , 128 , mbr_ops);
		if(feof (mbr_ops) || strlen(cmd) < 3)
			break;

		get_cmd(params , cmd);

		if (strcmp (params[0] , "RENAME_DIR") == 0)
			drename (params[1] , params[2]);
		
		if (strcmp (params[0] , "DELETE_DIR") == 0)
			ddelete (params[1]);

		if (strcmp (params[0] , "CREATE_DIR") == 0)
			dcreate (params[1]);

		if (strcmp (params[0] , "PRINT") == 0)
			dprint (params[1]);

		if (strcmp (params[0] , "RENAME_FILE") == 0)
			frename (params[1] , params[2]);

		if (strcmp (params[0] , "DELETE_FILE") == 0)
			fdelete (params[1]);

		if (strcmp (params[0] , "CREATE_FILE") == 0)
			fcreate (params[1]);
	} 
}
// -=-=-=-=-=-=-=-=- ~~~ Task 3 ~~~ =-=-=-=-=-=-=-=-=-=-
if (atoi(argv[1]) == 3)
{
	FILE *mbr_ops = fopen ("mbr.op" , "r");
	char  params [16][128] , cmd[4096];

	while (1)
	{
		fgets (cmd , 4096 , mbr_ops);
		if (strlen (cmd) < 2) 
			break;

		get_cmd(params , cmd);

		if(feof (mbr_ops) )
			break;

		if (strcmp (params[0] , "CREATE_PARTITION") == 0)
			pcreate (params[1] , atoi (params[2]) , atoi (params[3]));

		if (strcmp (params[0] , "FORMAT_PARTITION") == 0)
			pformat (params[1]);

		if (strcmp (params[0] , "DELETE_PARTITION") == 0)
			pdelete (params[1]);
	} 
}
// -=-=-=-=-=-=-=-=- ~~~ Task 4 ~~~ =-=-=-=-=-=-=-=-=-=-
if (atoi(argv[1]) == 4)
{
	FILE *mbr_ops = fopen ("mbr.op" , "r");
	FILE *mbr = fopen ("mbr.bin" , "r");

	part temp_part;
	char path[9] , cmp[9] , ch;
	int x , mbr_size = 0 , offset[128] , cnt , zece = 9 , nr_linii = 0 , ji;

	while(!feof(mbr_ops))
	{
  		ch = fgetc(mbr_ops);
  		if(ch == '/')
    		nr_linii++;
	}
	nr_linii /= 2;

	fseek(mbr_ops , 0 , SEEK_SET);
	for (ji = 1 ; ji <= nr_linii ; ji++)
	{
		fseek(mbr , 0 , SEEK_SET);
		fscanf (mbr_ops , "%s" , path); 
		x = 0; cnt = 0;
		while (x!= 8)
		{
			fread(&x , sizeof(int) , 1 , mbr);
			cnt += 4;
		}
		cnt -= 4;
		fseek(mbr , -4 , SEEK_CUR);
		while (!feof (mbr))
		{
			fread(&temp_part , sizeof(part) , 1 , mbr);

			strcpy (cmp , temp_part.head.nume);
			cmp [8] = '\0';
			
			if (strcmp (path , cmp) == 0)
			{
				mbr_size ++;
				offset [mbr_size] = cnt;
				break;
			}

			x = 20 + 12 * (temp_part.maxd + (temp_part.maxd + 1)*temp_part.maxf);
			cnt += x;

			if (feof(mbr))
				break;

			fseek (mbr , x - 20 , SEEK_CUR);
		}
		strcpy (path , cmp);
	} 
	
	printf("MBR_SIZE: %d\n", mbr_size);
	printf("PARTITIONS_OFFSET:");
	for (ji = 1 ; ji <= mbr_size ; ji++)
		printf(" %d", offset[ji]);

	fclose(mbr_ops);
	fclose(mbr);
}
	return 0;
}
