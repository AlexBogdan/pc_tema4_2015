Toate tipurile de structuri ce pot aparea au fost definite la inceput pentru a putea citi mai usor din fisierul binar.

Mentionez inca de la inceput ca este mult copy-paste folosit in toata tema (unca in vacnata .. ) , asa ca voi explica aici totul.

-=-=-= Task 1 -=-=-=-=

Se va parcurge MBR in 3 foruri , primul este pentru fiecare partitie , al doilea pentru foldere si al 3-lea pentru fisiere.

Pentru a valida un MBR , se verifica daca prin parcurgerea fiecarui continut de partitie se va ajunge cu cursorul in dreptul adresei de inceput a urmatoarei partitie. Pe parcurs se salveaza si cerintele de output.


-=-=-=-= Task 2 -=-=-=

Functii mari .. Prima care a fost scrisa este 'drename' , iar in functie de aceasta , prin copy-paste au fost generate si restul.

-- 'drename' desface calea absoluta in 2 , path1 = numele partiei in care trebuie sa ajunga , path2 = numele fisierului cautat .. dupa cautam printr-o parcurgere simpla , iar daca gasim folderul : setam zona pe 0 , dupa rescriem dupa noul nume

-- 'ddelete' lucreaza intocmai ca mai sus , doar ca va umple cu 0 cei 12 bytes ai folderului specificat , daca acesta este gasit.

-- 'dcreate' copie dupa cele de mai sus .. Cauta prima zona de 0000 care ar trebui sa fie asociata unui folder in partitia respectiva si in caz ca aceasta exista , folderul este creat

-- 'drpint' foloseste structura lui ddelete , dar in momentul in care gaseste folderul , acceseaza maxf fisiere din el printr-un for si afiseaza ce gaseste.

--- Fucntiile de fisiere sunt ca cele de mai sus , doar ca au un for in plus pentru a cauta in respectivele foldere

-=-=-=-= Task 3 -=-=-=

-- pdelete cauta partitia ceruta si in cazul in care o gaseste , seteaza 'int-ul' din zona de MBR pe 0000

-- pdelete cauta partitia ceruta si in cazul in care o gaseste , seteaza tot continutul acesteia pe 0

-- pcreate salveaza intr-o strucutra pozitiile de start si dimensiunea fiecarei partitii existente , urmand ca punctele de 0 sa fie verificate pentru a decide daca este loc sau nu pentru respectiva noua partitie

-=-=-=-= Task 4 (Bonus) -=-=-=

Am ales sa numar in primul rand numarul de linii din 'mbr.op' pentru a stii cati pasi va afea acest task .. Unele fisiere de intrare avea \n in plus , etc. Dupa aceea am cautat prima partitie (care clar are dim = 8 ) si am inceput sa verific fiecare nume de partitie , sarind direct din header in header de partitie.
Outputurile se salveaza separat si sunt afisate la final
